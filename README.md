# PCBWay Plug-in for Kicad


### Send your layout to PCBWay for instant production with just one click.

Click this plug-in for high quality prototyping and assembly services, PCBWay will commit to meeting your needs to the greatest extent.

When you click PCBWay Plug-in button, we will export these files in your project：

1.Gerber files in correct format for production<br>
2.IPC-Netlist file<br>
3.Bom-file that includes all information of components<br>
4.Pick and Place-file used in assembly<br>

You can click "Save to Cart" to place an order immediately after uploading the files( usually only takes a few seconds), our engineers will double check the files before the production.

<img src="https://user-images.githubusercontent.com/20063837/160805517-c1e80546-4672-46cb-9d0a-65d71400459d.gif">

### Installation from the official KiCad repositories

Just open the "Plugin and Content Manager" from the KiCad main menu an install the "PCBWay Plug-in for KiCad" plugin from the selection list.

### Manual installatioManual installation

You can also download the latest ZIP file from https://github.com/pcbway/PCBWay-Plug-in-for-Kicad, then open the "Plugin and Content Manager" from the main window of KiCad and install the ZIP file via "Install from File".
<img src="https://user-images.githubusercontent.com/20063837/160970891-4971cb1a-a36a-45bc-b219-93924f0ff070.png">

### About Bom

We can get all information of components used in your design. In order to speed up the quotation of components, we need this information:

1.Designator (necessary)<br>
2.Quantity (necessary)<br>
3.MPN/Part Number (necessary)<br>
4.Package/Footprint (necessary)<br>
5.Manufacturer (optional)<br>
6.Description/value (optional)<br>

You just need to add the properties in your schematic like the picture shows:
<img src="https://user-images.githubusercontent.com/20063837/160999527-e0a50238-3468-44be-b691-667ab8e8fef1.png">

### About PCBWay

PCBWay is a Chinese company specializing in PCB industry, dedicated to reducing customers' worries by providing one-stop service. We have extremely high production efficiency, ordinary PCBs only take 24 hours to produce. And the material we use is TG 150-160 with the price as low as $5/10pcs. Meanwhile, PCBWay also offer remarkable after-sales service which can solve any minor problems you may encounter. Give us a chance and we will give you back a satisfactory result.

As a sponsor of KiCad, we will always support its development.

<img src="https://user-images.githubusercontent.com/20063837/160807492-241eaa2b-b97f-4cbc-9fa0-f5f96eafb018.png">
